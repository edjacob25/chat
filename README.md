# Chat
A simple application which hosts a chat, with a simple bot which check stocks.

## Architecture
Is a ASPNET Core MVC application, which uses a mix of REST APIs and SignalR to achieve interactivity. By default it uses a local SQLite Database to host the messages and chat information, but it can be easily adjusted to use another database by adding the nuget project and changing the defaults.

As of now, there exists an interface which must be implemented and registered to create new bots, so they run inside te same host. In the future it will be easy enough to migrate to different processes with different communication while only adapting `BotResolver` service.

As for the consumption of the RabbitMQ, we are using a `BackgroundService` which consumes the events on the Queue and the uses the `ChatHub` to send updates to the clients.

The endpoints in the `Chat` domain are protected behind ASP NET Identity and require to be logged in. As the SignalR endpoints are only in the client, they are also protected, although this can be refined. 

## Running 
First, you need to have NET 5 with ASPNET Core 5 and RabbitMQ installed.
Before running it, you also have to change the settings related to RabbitMQ in `appsettings.json` or `appsettings.Development.json`, while setting the password using 
```
dotnet user-secrets init
dotnet user-secrets set "RabbitMq:Password" "Password" 
```
After that has been taken care of, you can simply use `dotnet run`  and it should work in any platform.

When deploying to production, I would personally deactivate the SSL connection and use it behind a reverse proxy, such as `nginx`

## TODO
- Implement a cleaner and more separed architecture with the bots.
- Improve the UI
- Improve the hosted service
- Testing (Didn't have enough time)
- Code quality (Is the first time I have worked with some libs, so it took more time than iit was necessary)