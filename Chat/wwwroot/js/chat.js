"use strict";
const connection = new signalR.HubConnectionBuilder().withUrl("/chathub").build();

//Disable send button until connection is established
document.getElementById("sendButton").disabled = true;
const parser = new DOMParser();

connection.on("ReceiveMessage", function (html, name) {
    console.log("Receiving new message");
    const messages = document.getElementById("messages");
    const doc = parser.parseFromString(html, "text/html");
    const node = doc.body.children[0];
    if (name !== messages.dataset.main) {
        node.classList.remove("justify-content-end");
    }
    messages.append(node);
});

connection.on("ReceiveResult", function (html) {
    console.log("Receiving new result");
    const messages = document.getElementById("messages");
    const node = document.createElement('div');
    node.classList.add("card");
    node.append(document.createElement('div'));
    node.children[0].classList.add("card-body");
    node.children[0].innerText = html;
    messages.append(node);
});

connection.start().then(function () {
    document.getElementById("sendButton").disabled = false;
}).catch(function (err) {
    return console.error(err.toString());
});

async function sendChat() {
    const textArea = document.getElementById("chat_textarea");
    const text = textArea.value;

    if (text === "") {
        return;
    }

    await fetch('/Chat/PostMessage', {
        method: "POST",
        headers: {'Content-Type': 'application/json'},
        body: JSON.stringify({Text: text})
    }).then(response => {
        if (!response.ok) {
            throw new Error("Network error");
        }
        textArea.value = "";
    });
}


document.getElementById("sendButton").addEventListener("click", sendChat);