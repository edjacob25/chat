using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Chat.Models;
using Chat.Services;
using CsvHelper;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using RabbitMQ.Client;
using RabbitMQ.Client.Exceptions;

namespace Chat.Bots
{
    public class StocksBot : IBot
    {
        private readonly IHttpClientFactory _httpClientFactory;
        private readonly ILogger<StocksBot> _logger;
        private readonly IOptions<RabbitMqConfiguration> _options;
        private readonly Regex _regex = new(@"\/stock=(\w{2,10}(?:\.\w{2,10})?)");
        private readonly IConnection _connection;

        public StocksBot(IHttpClientFactory httpClientFactory, ILogger<StocksBot> logger,
            IOptions<RabbitMqConfiguration> options)
        {
            _httpClientFactory = httpClientFactory;
            _logger = logger;
            _options = options;

            try
            {
                var factory = new ConnectionFactory
                {
                    HostName = _options.Value.Hostname,
                    UserName = _options.Value.Username,
                    Password = _options.Value.Password
                };
                _connection = factory.CreateConnection();
            }
            catch (BrokerUnreachableException ex)
            {
                _logger.LogError("Could not connect to RabbitMQ");
            }
        }

        public Regex CommandRegex() => _regex;

        public async Task ExecuteCommand(string command)
        {
            var match = _regex.Match(command);
            var stock = match.Groups[1].Value;
            var query = new Dictionary<string, string?>
            {
                ["s"] = stock.ToLowerInvariant(),
                ["f"] = "sd2t2ohlcv",
                ["e"] = "csv",
                ["h"] = ""
            };
            var request = new HttpRequestMessage(HttpMethod.Get, QueryHelpers.AddQueryString("/q/l/", query));
            _logger.LogInformation(request.ToString());
            var httpClient = _httpClientFactory.CreateClient("stocks");

            var response = await httpClient.SendAsync(request);
            if (response.IsSuccessStatusCode)
            {
                var stream = await response.Content.ReadAsStreamAsync();
                using var streamReader = new StreamReader(stream);
                using var reader = new CsvReader(streamReader, CultureInfo.InvariantCulture);
                var records = reader.GetRecords<Stock>().ToList();
                _logger.LogInformation(records.Any()
                    ? $"{records.First().Symbol} quote is {records.First().Open} per share"
                    : $"Could not find {stock}");

                if (records.Any())
                {
                    using var channel = _connection.CreateModel();
                    channel.QueueDeclare(queue: _options.Value.Queue, durable: false, exclusive: false,
                        autoDelete: false, arguments: null);
                    var body = Encoding.UTF8.GetBytes(
                        $"{records.First().Symbol} quote is {records.First().Open} per share");

                    channel.BasicPublish(exchange: "", routingKey: _options.Value.Queue, basicProperties: null,
                        body: body);
                }
            }
        }

        private record Stock(string Symbol, string Date, string Time, string Open, string High, string Low,
            string Close, string Volume);
    }
}