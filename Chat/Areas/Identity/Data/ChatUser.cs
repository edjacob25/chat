using Microsoft.AspNetCore.Identity;

namespace Chat.Areas.Identity.Data
{
    // Add profile data for application users by adding properties to the ChatUser class
    public class ChatUser : IdentityUser
    {
        [PersonalData] public string ShowName { get; set; }
    }
}