using System;
using System.Linq;
using Microsoft.Extensions.DependencyInjection;

namespace Chat.Services
{
    public class BotResolver : IBotResolver
    {
        private readonly IServiceProvider _serviceProvider;

        public BotResolver(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        public IBot? ResolveCommand(string command)
        {
            var botServices = _serviceProvider.GetServices<IBot>();
            return botServices.FirstOrDefault(bot => bot.CommandRegex().IsMatch(command));
        }
    }
}