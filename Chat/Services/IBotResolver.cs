using Chat.Data;

namespace Chat.Services
{
    public interface IBotResolver
    {
        IBot? ResolveCommand(string command);
    }
}