using System;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using Chat.Hubs;
using Chat.Models;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using RabbitMQ.Client.Exceptions;

namespace Chat.Services
{
    public class MessageQueueReceiverService : BackgroundService
    {
        private readonly ILogger<MessageQueueReceiverService> _logger;
        private readonly IOptions<RabbitMqConfiguration> _options;
        private readonly IHubContext<ChatHub> _hubContext;
        private readonly IConnection? _connection;
        private readonly IModel? _channel;

        public MessageQueueReceiverService(ILogger<MessageQueueReceiverService> logger,
            IOptions<RabbitMqConfiguration> options, IHubContext<ChatHub> hubContext)
        {
            _logger = logger;
            _options = options;
            _hubContext = hubContext;
            try
            {
                var factory = new ConnectionFactory
                {
                    HostName = _options.Value.Hostname,
                    UserName = _options.Value.Username,
                    Password = _options.Value.Password
                };
                _connection = factory.CreateConnection();

                _channel = _connection.CreateModel();

                _channel.QueueDeclare(
                    queue: _options.Value.Queue,
                    durable: false,
                    exclusive: false,
                    autoDelete: false,
                    arguments: null);
            }
            catch (BrokerUnreachableException ex)
            {
                _logger.LogError("Could not connect to RabbitMQ");
            }
        }

        protected override Task ExecuteAsync(CancellationToken stoppingToken)
        {
            if (stoppingToken.IsCancellationRequested)
            {
                _channel.Dispose();
                _connection.Dispose();
                return Task.CompletedTask;
            }

            var consumer = new EventingBasicConsumer(_channel);

            consumer.Received += (model, ea) =>
            {
                var body = ea.Body.ToArray();

                var message = Encoding.UTF8.GetString(body);

                Task.Run(async () =>
                {
                    _logger.LogInformation($"Received message {message} from Rabbit");
                    await _hubContext.Clients.All.SendAsync("ReceiveResult", message);
                });
            };

            _channel.BasicConsume(queue: _options.Value.Queue, autoAck: true, consumer: consumer);

            return Task.CompletedTask;
        }
    }
}