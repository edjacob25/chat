using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Chat.Services
{
    public interface IBot
    {
        Regex CommandRegex();
        Task ExecuteCommand(string command);
    }
}