using System;
using Chat.Areas.Identity.Data;

namespace Chat.Models
{
    public class Message
    {
        public long Id { get; set; }
        public string Text { get; set; }
        public DateTime Timestamp { get; set; }
        public ChatUser Sender { get; set; }
    }
}