using System;

namespace Chat.Models
{
    public class RabbitMqConfiguration
    {
        public string? Hostname { get; set; }
        public string? Password { get; set; }
        public string? Queue { get; set; }
        public string? Username { get; set; }
    }
}