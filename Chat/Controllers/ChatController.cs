using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Chat.Areas.Identity.Data;
using Chat.Data;
using Chat.Extensions;
using Chat.Hubs;
using Chat.Models;
using Chat.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Logging;

namespace Chat.Controllers
{
    [Authorize]
    public class ChatController : Controller
    {
        private readonly ApplicationDbContext _dbContext;
        private readonly IHubContext<ChatHub> _hubContext;
        private readonly ILogger<ChatController> _logger;
        private readonly UserManager<ChatUser> _userManager;
        private readonly IBotResolver _botResolver;

        public ChatController(ILogger<ChatController> logger, UserManager<ChatUser> userManager,
            ApplicationDbContext dbContext, IHubContext<ChatHub> hubContext, IBotResolver botResolver)
        {
            _logger = logger;
            _userManager = userManager;
            _dbContext = dbContext;
            _hubContext = hubContext;
            _botResolver = botResolver;
        }


        public async Task<IActionResult> Index()
        {
            var chatUser = await _userManager.GetUserAsync(User);
            var messages = _dbContext.Messages.OrderByDescending(m => m.Timestamp).Take(50)
                .Select(m => new MessageViewModel(m.Sender.ShowName, m.Text, m.Timestamp, m.Sender == chatUser))
                .Reverse()
                .ToList();
            var model = new ChatViewModel(chatUser.ShowName, messages);
            return View(model);
        }


        [HttpPost]
        public async Task<IActionResult> PostMessage([FromBody] MessageIn msg)
        {
            if (msg.Text.StartsWith("/"))
            {
                var bot = _botResolver.ResolveCommand(msg.Text);
                if (bot != null)
                {
                    _logger.LogInformation($"Bot of type {bot.GetType()} is doing something");
                    // Not awaited because we don't really care about the result here, also, as the bots are singletons,
                    // they outlive the request scope
                    bot.ExecuteCommand(msg.Text);
                }
                else
                {
                    _logger.LogInformation($"Bot not found, sending to user {User.Identity.Name}");
                    var user = _hubContext.Clients.User(User.Identity.Name);
                    await user.SendAsync("ReceiveResult",
                        $"Could not find a bot which has the command: {msg.Text}");
                }

                return Ok();
            }

            var chatUser = await _userManager.GetUserAsync(User);

            var message = new Message
            {
                Sender = chatUser,
                Text = msg.Text,
                Timestamp = DateTime.Now
            };
            _logger.LogInformation($"Message from {message.Sender}");
            await _dbContext.Messages.AddAsync(message);
            await _dbContext.SaveChangesAsync();
            var viewModel = new MessageViewModel(message.Sender.ShowName, message.Text, message.Timestamp, true);
            var rendered = await this.RenderViewAsync("_MessagePartial", viewModel, true);
            await _hubContext.Clients.All.SendAsync("ReceiveMessage", rendered, viewModel.Username);
            return Ok();
        }
    }

    public record MessageIn(string Text);

    public record MessageViewModel(string Username, string Text, DateTime Date, bool IsOwnMessage);

    public record ChatViewModel(string Name, List<MessageViewModel> Messages);
}