using System.Threading.Tasks;
using Chat.Controllers;
using Chat.Services;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Logging;

namespace Chat.Hubs
{
    public class ChatHub : Hub<IChatClient>
    {
        private readonly ILogger<ChatHub> _logger;
        private readonly IRazorPartialToStringRenderer _renderer;

        public ChatHub(ILogger<ChatHub> logger, IRazorPartialToStringRenderer renderer)
        {
            _logger = logger;
            _renderer = renderer;
        }

        public async Task SendMessage(MessageViewModel messsage)
        {
            _logger.LogInformation("Rendering a partial");
            var rendered = await _renderer.RenderPartialToStringAsync("_MessagePartial", messsage);
            await Clients.All.ReceiveMessage(rendered);
        }
    }
}