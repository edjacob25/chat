using System.Threading.Tasks;

namespace Chat.Hubs
{
    public interface IChatClient
    {
        Task ReceiveMessage(string html);
    }
}